#include <algorithm>
#include <cassert>

#include "client_transaction.h"
#include "log.h"

std::string format_client_transaction_state(ClientTransactionState state) {
  switch (state) {
  case ClientTransactionState::NOT_STARTED:
    return "not_started";
  case ClientTransactionState::START_SENT:
    return "start_sent";
  case ClientTransactionState::OPEN:
    return "open";
  case ClientTransactionState::ROLLBACK_SENT:
    return "rollback_sent";
  case ClientTransactionState::ROLLED_BACK:
    return "rolled_back";
  case ClientTransactionState::ROLLED_BACK_BY_SERVER:
    return "rolled_back_by_server";
  case ClientTransactionState::COMMIT_SENT:
    return "commit_sent";
  case ClientTransactionState::COMMITTED:
    return "committed";
  default:
    return std::string("unknown(") + std::to_string(static_cast<int>(state)) +
           std::string(")");
  }
}

ClientTransaction::ClientTransaction(ActorId self, ClientTransactionSpec spec,
                                     const Discovery *discovery, IRetrier *rt)
    : self_(self), spec_(std::move(spec)), discovery_(discovery), rt_(rt) {}

void ClientTransaction::maybe_init_participant(ActorId id,
                                               Timestamp timestamp) {
  if (p_[id] != nullptr) {
    return;
  }
  p_[id] = std::make_unique<ClientTransactionParticipant>(self_, id, rt_);
  p_[id]->start_at_timestamp(timestamp, txid_, read_timestamp_);
}

std::tuple<int, int> ClientTransaction::completed_requests() const {
  auto ret = std::make_tuple(0, 0);
  for (const auto &p : p_) {
    std::get<0>(ret) += p.second->completed_gets();
    std::get<1>(ret) += p.second->completed_puts();
  }
  return ret;
}

void ClientTransaction::tick(Timestamp timestamp,
                             const std::vector<Message> &messages,
                             std::vector<Message> *msg_out) {
  // Put messages handled by ClientTransaciton, into "msg_this".
  std::vector<Message> msg_this;

  std::unordered_map<ActorId, std::vector<Message>> actor_messages;
  for (const auto &msg : messages) {
    switch (msg.type) {
    case MSG_START_ACK:
    case MSG_GET_REPLY:
    case MSG_PUT_REPLY: {
      actor_messages[msg.source].push_back(msg);
      break;
    }
    default: {
      msg_this.push_back(msg);
    }
    }
  }

  for (auto &entry : p_) {
    entry.second->process_incoming(timestamp, actor_messages[entry.first]);
  }

  switch (state_) {
  case ClientTransactionState::NOT_STARTED: {
    process_replies_not_started(messages);
    if (timestamp >= spec_.earliest_start_timestamp) {
      coordinator_ = discovery_->get_random();
      p_[coordinator_] = std::make_unique<ClientTransactionParticipant>(
          self_, coordinator_, rt_);
      p_[coordinator_]->start(timestamp);
      txid_ = p_[coordinator_]->txid();
      LOG_DEBUG << "[tx " << txid_ << "] client " << self_ << " coordinator is "
                << coordinator_;
      state_ = ClientTransactionState::START_SENT;
    }
    break;
  }

  case ClientTransactionState::START_SENT: {
    process_replies_start_sent(msg_this);

    if (p_[coordinator_]->is_open()) {
      read_timestamp_ = p_[coordinator_]->read_timestamp();
      state_ = ClientTransactionState::OPEN;
    }
    break;
  }

  case ClientTransactionState::OPEN: {
    process_replies_open(msg_this);
    if (next_get_ < spec_.gets.size() &&
        timestamp >= spec_.gets[next_get_].earliest_timestamp) {
      auto key = spec_.gets[next_get_].key;
      ActorId target = discovery_->get_by_key(key);
      maybe_init_participant(target, timestamp);
      p_[target]->issue_get(key);
      ++next_get_;
    }
    if (next_put_ < spec_.puts.size() &&
        timestamp >= spec_.puts[next_put_].earliest_timestamp) {
      auto put_spec = spec_.puts[next_put_];
      auto key = put_spec.key;
      ActorId target = discovery_->get_by_key(key);
      maybe_init_participant(target, timestamp);
      p_[target]->issue_put(key, put_spec.value);

      ++next_put_;
    }

    auto cr = completed_requests();
    if (std::get<0>(cr) == spec_.gets.size() &&
        std::get<1>(cr) == spec_.puts.size() &&
        timestamp >= spec_.earliest_commit_timestamp) {
      // Issue a commit.
      if (spec_.action == ClientTransactionSpec::COMMIT) {
        auto msg = CreateMessageCommit(self_, coordinator_, txid_,
                                       get_participants_except_coordinator());
        commit_handle_ = rt_->schedule(timestamp, msg);
        state_ = ClientTransactionState::COMMIT_SENT;
      } else if (spec_.action == ClientTransactionSpec::ROLLBACK) {

        for (const auto &entry : p_) {
          auto msg = CreateMessageRollback(self_, entry.first, txid_);
          rollback_handle_[entry.first] = rt_->schedule(timestamp, msg);
        }
        state_ = ClientTransactionState::ROLLBACK_SENT;
      } else {
        LOG_ERROR << "Invalid transaction spec";
        exit(EXIT_FAILURE);
      }
    }
    break;
  }

  case ClientTransactionState::COMMIT_SENT: {
    process_replies_commit_sent(msg_this);
    break;
  }

  case ClientTransactionState::ROLLBACK_SENT: {
    process_replies_rollback_sent(msg_this);
    break;
  }

  case ClientTransactionState::COMMITTED: {
    process_replies_commited(msg_this);
    break;
  }

  case ClientTransactionState::ROLLED_BACK: {
    process_replies_rolled_back(msg_this);
    break;
  }

  case ClientTransactionState::ROLLED_BACK_BY_SERVER: {
    process_replies_rolled_back_by_server(msg_this);
    break;
  }
  }

  for (auto &entry : p_) {
    entry.second->maybe_issue_requests(timestamp);
  }

  rt_->get_ready(timestamp, msg_out);
}

void ClientTransaction::process_replies_not_started(
    const std::vector<Message> &messages) {
  // No incoming messages expected in this state.
  for (const auto &msg : messages) {
    report_unexpected_msg(msg);
  }
}

void ClientTransaction::process_replies_start_sent(
    const std::vector<Message> &messages) {
  for (const auto &msg : messages) {
    switch (msg.type) {
    case MSG_ROLLED_BACK_BY_SERVER: {
      handle_rolled_back_by_server(msg);
      break;
    }
    default: {
      report_unexpected_msg(msg);
    }
    }
  }
}

void ClientTransaction::handle_rolled_back_by_server(const Message &msg) {
  state_ = ClientTransactionState::ROLLED_BACK_BY_SERVER;
  conflict_txid_ = msg.get<MessageRolledBackByServerPayload>().conflict_txid;
}

void ClientTransaction::process_replies_open(
    const std::vector<Message> &messages) {
  for (const auto &msg : messages) {
    switch (msg.type) {
    case MSG_ROLLED_BACK_BY_SERVER: {
      // final state.
      handle_rolled_back_by_server(msg);
      break;
    }
    default: {
      report_unexpected_msg(msg);
    }
    }
    if (state_ == ClientTransactionState::ROLLED_BACK_BY_SERVER) {
      break;
    }
  }
}

void ClientTransaction::process_replies_commit_sent(
    const std::vector<Message> &messages) {
  for (const auto &msg : messages) {
    switch (msg.type) {
    case MSG_COMMIT_ACK: {
      state_ = ClientTransactionState::COMMITTED;
      commit_timestamp_ = msg.get<MessageCommitAckPayload>().commit_timestamp;
      rt_->cancel(commit_handle_);
      break;
    }
    case MSG_ROLLED_BACK_BY_SERVER: {
      handle_rolled_back_by_server(msg);
      rt_->cancel(commit_handle_);
      break;
    }
    default: {
      report_unexpected_msg(msg);
    }
    }
  }
}

void ClientTransaction::process_replies_rollback_sent(
    const std::vector<Message> &messages) {
  for (const auto &msg : messages) {
    switch (msg.type) {
    case MSG_ROLLBACK_ACK: {
      auto source = msg.source;
      auto iter = rollback_handle_.find(source);
      if (iter != rollback_handle_.end()) {
        rt_->cancel(iter->second);
        rollback_handle_.erase(iter);
      }
      if (rollback_handle_.empty()) {
        state_ = ClientTransactionState::ROLLED_BACK;
      }
      break;
    }
    default: {
      report_unexpected_msg(msg);
    }
    }
  }
}

void ClientTransaction::process_replies_commited(
    const std::vector<Message> &messages) {
  for (const auto &msg : messages) {
    switch (msg.type) {
    case MSG_COMMIT_ACK: {
      // Ignore duplicates.
      break;
    }
    case MSG_ROLLED_BACK_BY_SERVER: {
      LOG_ERROR << "[txid = " << txid_
                << "] (in state committed) Internal transaction error: "
                << "ServerTransaction should never "
                << "send both CommitAck, and RolledBackByServer";
      exit(EXIT_FAILURE);
    }
    default: {
      report_unexpected_msg(msg);
    }
    }
  }
}

void ClientTransaction::process_replies_rolled_back(
    const std::vector<Message> &messages) {
  for (const auto &msg : messages) {
    switch (msg.type) {
    case MSG_ROLLBACK_ACK: {
      // Ignore duplicates.
      break;
    }
    case MSG_ROLLED_BACK_BY_SERVER: {
      LOG_ERROR << "[txid = " << txid_ << "] (in state rolled back) "
                << "Internal transaction error: ServerTransaction should never "
                << "send both RollbackAck, and RolledBackByServer";
      exit(EXIT_FAILURE);
    }
    default: {
      report_unexpected_msg(msg);
    }
    }
  }
}

void ClientTransaction::process_replies_rolled_back_by_server(
    const std::vector<Message> &messages) {
  for (const auto &msg : messages) {
    switch (msg.type) {
    case MSG_ROLLED_BACK_BY_SERVER:
    case MSG_ROLLBACK_ACK: {
      // Ignore duplicates.
      break;
    }
    case MSG_COMMIT_ACK: {
      LOG_ERROR << "[txid = " << txid_
                << "] Internal transaction error: ServerTransaction "
                << " should never send CommitAck after RolledBackByServer";
      exit(EXIT_FAILURE);
    }
    default: {
      report_unexpected_msg(msg);
    }
    }
  }
}

void ClientTransaction::report_unexpected_msg(const Message &msg) {
  LOG_ERROR << "[tx " << txid_ << "] Node " << msg.source
            << " returns unexpected message of type \""
            << format_message_type(msg.type)
            << "\" for client transaction in state " << (int)state_;
}

std::vector<ActorId>
ClientTransaction::get_participants_except_coordinator() const {
  std::vector<ActorId> participants;
  for (const auto &[id, p] : p_) {
    if (id != coordinator_) {
      participants.push_back(id);
    }
  }
  return participants;
}

ClientTransactionResults ClientTransaction::export_results() const {
  ClientTransactionResults ret;
  ret.txid = txid_;
  ret.read_timestamp = read_timestamp_;
  ret.commit_timestamp = commit_timestamp_;

  for (const auto &p : p_) {
    p.second->export_results(&ret.gets, &ret.puts);
  }

  return ret;
}

TransactionId ClientTransaction::get_id() const { return txid_; }
