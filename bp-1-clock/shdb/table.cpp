#include "table.h"

namespace shdb {

class ImplementedTable : public Table
{
    std::shared_ptr<BufferPool> buffer_pool;
    std::shared_ptr<File> file;
    PageProvider page_provider;
    PageIndex current_page_index = 0;

public:
    ImplementedTable(std::shared_ptr<BufferPool> buffer_pool, std::shared_ptr<File> file, PageProvider page_provider)
        : buffer_pool(std::move(buffer_pool)), file(std::move(file)), page_provider(std::move(page_provider))
    {}

    virtual RowId insert_row(const Row &row) override
    {
        while (true) {
            if (current_page_index == get_page_count()) {
                current_page_index = file->alloc_page();
            }
            auto page = get_page(current_page_index);
            if (auto [found, row_index] = page->insert_row(row); found) {
                return RowId{current_page_index, row_index};
            }
            ++current_page_index;
        }
    }

    virtual Row get_row(RowId row_id) override
    {
        auto page = get_page(row_id.page_index);
        auto row = page->get_row(row_id.row_index);
        return row;
    }

    virtual void delete_row(RowId row_id) override
    {
        auto page = get_page(row_id.page_index);
        page->delete_row(row_id.row_index);
    }

    virtual PageIndex get_page_count() override
    {
        return file->get_page_count();
    }

    virtual std::shared_ptr<Page> get_page(PageIndex page_index) override
    {
        auto frame = buffer_pool->get_page(file, page_index);
        return page_provider(std::move(frame));
    }
};

std::shared_ptr<Table>
    create_table(std::shared_ptr<BufferPool> buffer_pool, std::shared_ptr<File> file, PageProvider page_provider)
{
    return std::make_shared<ImplementedTable>(std::move(buffer_pool), std::move(file), std::move(page_provider));
}

}    // namespace shdb
