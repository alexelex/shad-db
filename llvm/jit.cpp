#include "jit.h"

#include <llvm/ADT/StringRef.h>
#include <llvm/ExecutionEngine/JITSymbol.h>
#include "llvm/ExecutionEngine/Orc/CompileUtils.h"
#include "llvm/ExecutionEngine/Orc/Core.h"
#include "llvm/ExecutionEngine/Orc/ExecutionUtils.h"
#include "llvm/ExecutionEngine/Orc/IRCompileLayer.h"
#include "llvm/ExecutionEngine/Orc/JITTargetMachineBuilder.h"
#include "llvm/ExecutionEngine/Orc/RTDyldObjectLinkingLayer.h"
#include "llvm/ExecutionEngine/SectionMemoryManager.h"

#include "llvm/IR/DataLayout.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Verifier.h"

#include <memory>
#include <mutex>

namespace shdb {

using namespace llvm;
using namespace llvm::orc;
using llvm::Value;

class Jit::JIT
{
    ExecutionSession ES;
    RTDyldObjectLinkingLayer ObjectLayer;
    IRCompileLayer CompileLayer;

    DataLayout DL;
    MangleAndInterner Mangle;
    ThreadSafeContext Ctx;

    JITDylib &MainJD;

public:
    JIT(JITTargetMachineBuilder JTMB, DataLayout DL)
        : ObjectLayer(ES, [] () { return std::make_unique<SectionMemoryManager>();})
        , CompileLayer(ES, ObjectLayer, ConcurrentIRCompiler(std::move(JTMB)))
        , DL(std::move(DL))
        , Mangle(ES, this->DL)
        , Ctx(std::make_unique<LLVMContext>())
        , MainJD(ES.createJITDylib("<main>"))
    { }

    void registerSymbol(uintptr_t address, StringRef name)
    {
        SymbolMap M;
        M[Mangle(name)] = JITEvaluatedSymbol(static_cast<JITTargetAddress>(address), JITSymbolFlags());
        cantFail(MainJD.define(absoluteSymbols(M)));
    }

    static std::unique_ptr<JIT> Create()
    {
        static std::once_flag flag;
        std::call_once(flag, []{
            LLVMInitializeNativeTarget();
            LLVMInitializeNativeAsmPrinter();
            LLVMInitializeNativeAsmParser();
        });

        auto JMTB = cantFail(JITTargetMachineBuilder::detectHost());
        auto DL = cantFail(JMTB.getDefaultDataLayoutForTarget());
        return std::make_unique<JIT>(std::move(JMTB), std::move(DL));
    }

    const DataLayout &getDataLayout() const { return DL; }

    LLVMContext &getContext() { return *Ctx.getContext(); }

    void addModule(std::unique_ptr<Module> M) {
        cantFail(CompileLayer.add(MainJD, ThreadSafeModule(std::move(M), Ctx)));
    }

    JITEvaluatedSymbol lookup(StringRef Name) {
        return cantFail(ES.lookup({&MainJD}, Mangle(Name.str())));
    }
};


Jit::Jit()
    : jit(JIT::Create())
    , ctx(jit->getContext())
    , builder(IRBuilder<>(ctx))
    , module(std::make_unique<Module>("jit module", ctx))
    , void_type(Type::getVoidTy(ctx))
    , i8_type(Type::getInt8Ty(ctx))
    , i32_type(Type::getInt32Ty(ctx))
    , i64_type(Type::getInt64Ty(ctx))
    , i8ptr_type(PointerType::getUnqual(i8_type))
{
    module->setDataLayout(jit->getDataLayout());
}

Jit::~Jit() = default;

void Jit::finish(bool dump)
{
    verifyModule(*module.get());

    if (dump) {
        module->print(llvm::errs(), nullptr);
    }

    jit->addModule(std::move(module));
}

void Jit::register_symbol(uintptr_t address, StringRef name)
{
    jit->registerSymbol(address, name);
}

Function* Jit::create_function(StringRef name, Type* result, std::vector<Type*> arguments)
{
    return Function::Create(
        FunctionType::get(result, arguments, false),
        Function::ExternalLinkage,
        name,
        module.get());
}

BasicBlock* Jit::create_bb(StringRef name, Function* function)
{
    return BasicBlock::Create(ctx, name, function);
}

Value* Jit::create_constant(unsigned width, uint64_t value)
{
    return ConstantInt::get(ctx, APInt(width, value));
}

uintptr_t Jit::get_compiled_raw(StringRef name)
{
    return static_cast<uintptr_t>(jit->lookup(name).getAddress());
}

}
