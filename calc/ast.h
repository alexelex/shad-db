#pragma once

#include <string>
#include <memory>

namespace calc {

enum class Type {
    number,
    binary,
    unary,
};

enum class Opcode {
    plus,
    minus,
    mul,
    div,
    uminus,
};

struct Ast
{
    Type type;

    Ast(Type type);
    virtual ~Ast() = default;
};

struct Number
    : public Ast
{
    int value;

    explicit Number(int value);
};

struct Binary
    : public Ast
{
    Opcode op;
    std::shared_ptr<Ast> lhs;
    std::shared_ptr<Ast> rhs;

    Binary(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs);
};

struct Unary
    : public Ast
{
    Opcode op;
    std::shared_ptr<Ast> operand;

    Unary(Opcode op, std::shared_ptr<Ast> operand);
};

std::shared_ptr<Ast> new_number(int value);
std::shared_ptr<Ast> new_binary(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast>rhs);
std::shared_ptr<Ast> new_unary(Opcode op, std::shared_ptr<Ast> operand);

std::string to_string(std::shared_ptr<Ast> ast);

}
