
#include "retrier.h"

extern void TestStorageSimple();
extern void TestClientTransactionSimple();
extern void TestClientServerTransaction(
  double drop_probability, IRetrier* client_retrier,
  IRetrier* server_retrier);

extern void TestClientServerTwoConflictingTransactions(
  double drop_probability, IRetrier* retrier[4]);

int main() {
  TestClientTransactionSimple();
  TestStorageSimple();
  
  double drop_probability = 0.4;
  
  RetrierExpBackoff client_retrier(50);
  RetrierExpBackoff server_retrier(150);

  TestClientServerTransaction(
      drop_probability, &client_retrier, &server_retrier);

  RetrierExpBackoff brt[4] = {
      RetrierExpBackoff(20), RetrierExpBackoff(30),
      RetrierExpBackoff(40), RetrierExpBackoff(50)};


  IRetrier* irt[4];

  for (int i = 0; i < 4; i++) {
    irt[i] = &brt[i];
  }

  TestClientServerTwoConflictingTransactions(drop_probability, irt);

  return 0;
}
