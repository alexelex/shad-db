package main

import (
  "fmt"
  "net"
  "log"
  "math/rand"
  "os"
  "sync"
  "time"
)

type DepSpec struct {
  int `json:"dep_on"`
  QueryIndex int `json:"query_index"`
}

type ClientBatchSpec struct {
  NumClients int `json:"num_clients"`
  NumReads int `json:"num_reads"`
  NumWrites int `json:"num_writes"`
  BatchSize int `json:"batch_size"`
  KeyRangeStart int `json:"key_range_start"`
  KeyRangeLimit int `json:"key_range_limit"`
  ValueRangeStart int `json:"value_range_start"`
  ValueRangeLimit int `json:"value_range_limit"`
  Seed int `json:"seed"`
  Deps []DepSpec `json:"deps"`
}

// The server is terminated after each round.
type RoundSpec struct {
  ClientBatch []ClientBatchSpec `json:"client_batch"`
}

func connect(port int) net.Conn {
  startTime := time.Now()
  backoff := 10 * time.Millisecond
  for {
    conn, err := net.Dial("tcp", fmt.Sprintf("127.0.0.1:%d", port))
    if err == nil {
      return conn
    }
    if time.Now().Sub(startTime) > 10 * time.Second {
      log.Printf("Unable to connect: %s", err.Error())
    }
    if time.Now().Sub(startTime) > 20 * time.Second {
      log.Fatalf("Unable to connect: %s", err.Error())
    }
    time.Sleep(backoff)
    backoff = 2 * backoff
    if backoff > 2 * time.Second {
      backoff = 2 * time.Second
    }
  }
}

func connWrite(val string, conn net.Conn) {
  buf := []byte(val)
  i := 0
  for i < len(buf) {
    n, err := conn.Write(buf[i:])
    if err != nil {
      fmt.Fprintf(os.Stderr, "Write error (%d bytes written) %s\n", n, err.Error())
    }
    i += n
  }
}

func runSingleClient(port int, seed int64, m* queriesManager, spec* ClientBatchSpec, wg* sync.WaitGroup) {
  defer wg.Done()

  conn := connect(port)
  r := rand.New(rand.NewSource(seed))

  n := spec.NumReads + spec.NumWrites
  rw := make([]bool, n)
  for i := 0; i < spec.NumWrites; i++ {
    rw[i] = true
  }

  r.Shuffle(n, func(i, j int) {
    tmp := rw[i]
    rw[i] = rw[j]
    rw[j] = tmp
  })

  qResults := make(chan string)

  go func() {
    nRead := 0
    cur := ""
    buf := make([]byte, 1024)
    for nRead < n {
      nBytes, err := conn.Read(buf)
      if err != nil {
        fmt.Fprintf(os.Stderr, "Only read %d bytes: %s\n", nBytes, err.Error())
      }

      for i := 0; i < nBytes; i++ {
        if buf[i] == '\n' {
          //log.Print("got result ", cur)
          qResults <- cur
          cur = ""
          nRead++
        } else {
          cur += string(buf[i])
        }
      }
    }
  }()

  nResults := 0
  queries := make([]interface{}, 0, len(rw))

  for i := 0; i < len(rw); i++ {
    for nResults < i - spec.BatchSize {
      result := <-qResults
      if rw[nResults] {
        queries[nResults].(*writeQuery).completed(result)
      } else {
        queries[nResults].(*readQuery).completed(result)
      }
      nResults++
    }

    var val string
    key := r.Intn(spec.KeyRangeLimit - spec.KeyRangeStart) + spec.KeyRangeStart
    qm := m.get(key)
    if rw[i] {
      value := fmt.Sprintf("val%d", r.Intn(spec.ValueRangeLimit - spec.ValueRangeStart) + spec.ValueRangeStart)
      val = fmt.Sprintf("PUT %d %s\n", key, value)
      //log.Print("write ", val)
      w := qm.startWrite(value)
      queries = append(queries, interface{}(w))
    } else {
      val = fmt.Sprintf("GET %d\n", key)
      //log.Print("read ", val)
      r := qm.startRead()
      queries = append(queries, interface{}(r))
    }
    connWrite(val, conn)
  }

  for nResults < n {
    result := <-qResults
    //log.Print(result, rw[nResults])
    if rw[nResults] {
      queries[nResults].(*writeQuery).completed(result)
    } else {
      queries[nResults].(*readQuery).completed(result)
    }
    nResults++
  }

  conn.Close()
}

func runClientBatch(port int, m* queriesManager, spec* ClientBatchSpec) {
  wg := &sync.WaitGroup{}
  wg.Add(spec.NumClients)
  r := rand.New(rand.NewSource(int64(spec.Seed)))
  for c := 0; c < spec.NumClients; c++ {
    go runSingleClient(port, r.Int63(), m, spec, wg)
  }
  wg.Wait()
}

func runRound(port int, m *queriesManager, spec* RoundSpec) {
  for _, cb := range spec.ClientBatch {
    runClientBatch(port, m, &cb)
  }
}
