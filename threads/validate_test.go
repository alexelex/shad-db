package main

import (
  "testing"
)

func TestValidateEmpty(t* testing.T) {
  val0 := "0"
  r := &readQuery{i: interval{begin: int64(1), end: int64(2)}, value: &val0}
  writes := []*writeQuery{}

  v := &validator{}
  v.init(writes)
  result := v.validate(r)

  if result == nil {
    t.Fatalf("Expected an error")
  }
}

func TestValidateOneWrite(t* testing.T) {
  val0 := "x"
  r := &readQuery{
      i: interval{begin: int64(10), end: int64(20)},
      value: &val0,
    }

  writes := []*writeQuery{
    &writeQuery{
      i: interval{begin: int64(1), end: int64(2)},
      value: val0,
    },
  }

  v := &validator{}
  v.init(writes)
  result := v.validate(r)

  if result != nil {
    t.Fatalf("Unexpected error %s", result)
  }
}

func TestValidateMultipleWrites(t *testing.T) {
  vals := []string{"0", "1", "2", "3", "4", "5"}

  writes := []*writeQuery{
    &writeQuery{
      i: interval{begin: int64(1), end: int64(2)},
      value: vals[0],
    },

    &writeQuery{
      i: interval{begin: int64(3), end: int64(5)},
      value: vals[1],
    },

    &writeQuery{
      i: interval{begin: int64(6), end: int64(8)},
      value: vals[2],
    },

    &writeQuery{
      i: interval{begin: int64(7), end: int64(10)},
      value: vals[3],
    },

    &writeQuery{
      i: interval{begin: int64(7), end: int64(20)},
      value: vals[4],
    },

    &writeQuery{
      i: interval{begin: int64(16), end: int64(20)},
      value: vals[5],
    },
  }

  v := &validator{}
  v.init(writes)

  valsValid := []string{"2", "3", "4"}

  for _, val := range(valsValid) {
    r := &readQuery{
        i: interval{begin: int64(8), end: int64(15)},
        value: &val,
      }

    err := v.validate(r)

    if err != nil {
      t.Fatalf("Unexpected error: %s", err.Error())
    }
  }
  valsInvalid := []string{"0", "1", "5"}

  for _, val := range(valsInvalid) {
    r := &readQuery{
      i: interval{begin: int64(8), end: int64(15)},
      value: &val,
    }

    err := v.validate(r)

    if err == nil {
      t.Fatalf("Expected an error for val %s", val)
    }
  }
}

func TestRegression0(t* testing.T) {
  val5 := "val5"
  r := &readQuery{value: &val5, i: interval{begin: 621, end:984}}

  vals := []string{
  "undefined", "val8", "val4", "val5", "val8", "val4", "val9", "val2", "val3", "val3",
  "val5", "val4", "val6", "val9",
  }

  begin := []int64{
  -10, 0, 22, 147, 154, 612, 831, 854, 873, 1172, 1208, 2022, 2673, 2812,
  }

  end := []int64{
  -9, 115, 158, 839, 598, 1554, 1056, 1086, 987, 1241, 2256, 2748, 2790, 2894,
  }

  n := len(vals)
  var writes []*writeQuery

  for i := 0; i < n; i++ {
    writes = append(writes, &writeQuery{
      i : interval{begin: begin[i], end: end[i]},
      value: vals[i],
    })
  }

  v := &validator{}
  v.init(writes)
  err := v.validate(r)

  if err != nil {
    t.Fatal(err.Error())
  }
/*
w 'undefined' [-1613171061888182, -1613171061888181)
w 'val8' [0, 115)
w 'val4' [22, 158)
w 'val5' [147, 839)
w 'val8' [154, 598)
w 'val4' [612, 1554)
w 'val9' [831, 1056)
w 'val2' [854, 1086)
w 'val3' [873, 987)
w 'val3' [1172, 1241)
w 'val5' [1208, 2256)
w 'val4' [2022, 2748)
w 'val6' [2673, 2790)
w 'val9' [2812, 2894)
  */
}

func TestRegression1(t *testing.T) {
  vals := []string{
    "undefined", "val6", "val5", "val1", "val9", "val6", "val2", "val4", "val6",
    "val9", "val8", "val9", "val7", "val6", "val0", "val4",
  }

  begin := []int64{
    -10, 0, 129, 2648, 259, 342, 974, 1240, 1310, 1614, 3323, 3531, 3830, 4006, 4167, 4383,
  }

  end := []int64{
    -9, 683, 663, 301, 317, 734, 2117, 1370, 2173, 4130, 3540, 3611, 4215, 4275, 4272, 4519,
  }

  vread := "val6"
  r := &readQuery{i: interval{begin: int64(4358), end:int64(4421)}, value: &vread}

  n := len(vals)
  var writes []*writeQuery

  for i := 0; i < n; i++ {
    writes = append(writes, &writeQuery{
      i : interval{begin: begin[i], end: end[i]},
      value: vals[i],
    })
  }

  v := &validator{}
  v.init(writes)
  err := v.validate(r)

  if err != nil {
    t.Fatal(err.Error())
  }
}

/*
r 'val3' [4513, 9329)

w 'undefined' [-16151264479425225, -16151264479425224)
w 'val4' [0, 4471)
w 'val3' [3281, 6418)
*/

func TestRegression2(t * testing.T) {
  vread := "val3"
  r := &readQuery{i: interval{begin: int64(4513), end:int64(9329)}, value: &vread}

  begin := []int64{-16151264479425225, 0, 3281}
  end := []int64{-16151264479425224, 4471, 6418}
  vals := []string{"undefined", "val4", "val3"}
  var writes []*writeQuery
  n := len(vals)

  for i := 0; i < n; i++ {
    writes = append(writes, &writeQuery{
      i : interval{begin: begin[i], end: end[i]},
      value: vals[i],
    })
  }

  v := &validator{}
  v.init(writes)
  err := v.validate(r)

  if err != nil {
    t.Fatal(err.Error())
  }
}
